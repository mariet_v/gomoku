//
// Gomoku.hh for Gomoku in /home/leniglo/Projects/ai-gomoku
//
// Made by Lefrant Guillaume
// Login   <leniglo@epitech.net>
//
// Started on  Tue Oct 21 18:16:30 2014 Lefrant Guillaume
// Last update Sun Jan  4 19:24:11 2015 Lazio
//

#ifndef			GOMOKU_HH_
# define		GOMOKU_HH_

# include <vector>
# include <sstream>
# include "LoadTexture.hh"
# include "Cell.hh"
# include "Arbiter.hh"
# include "AI.hh"

class			Gomoku
{
public:
  Gomoku(int ac, char **av);
  ~Gomoku();
  bool			runInterface();
  bool			runGame();
  sf::RenderWindow	_window;

private:
  bool			gestEventInterface();
  bool			displayInterface();
  bool			displayTextureInterface(eTextureInterface, int, int);
  bool			gestEventGame();
  bool			displayGame();
  bool			displayPawn();
  bool			displayTextureGame(eTextureGame, int, int);
  void			take(int index);
  LoadTexture		*_textures;
  Arbiter		*_arb;
  AI			*_ai;
  std::vector<Cell *>	_map;
  sf::Font		_font;
  sf::Text		_redText;
  sf::Text		_blueText;
  sf::Text		_yourTurnBlue;
  sf::Text		_yourTurnRed;
  bool			_blueTurn;
  bool			_exit;
  bool			_isOver;
  bool			_blueWin;
  bool			_pvp;
  int			_maxAITime;
};

#endif			/* !GOMOKU_HH_ */
