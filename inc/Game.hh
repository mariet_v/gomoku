//
// Game.hh for Game in /home/Lazio/celem/ai-gomoku/inc
// 
// Made by Lazio
// Login   <Lazio@linux.site>
// 
// Started on Thu Jan  1 17:19:53 2015 Lazio
// Last update Fri Jan  2 15:32:17 2015 Lazio
//

#ifndef GAME_HH_
# define GAME_HH_
# include <vector>
# include <stdlib.h>
# include "Cell.hh"
# include "Spot.hh"

class Game
{
public:
  Game(std::vector<Cell *> &map, Spot *spot);
  ~Game();
  bool	simulate(void);
  void	reset();
  bool	checkWin(void);
  bool	isThisValid(int x, int y);
  void	take(int x, int y);

private:
  bool			_myTurn;
  bool			_aiWin;
std::vector<Cell *>	_cellmap;
  Spot *		_spot;
  int			_map[19][19];
  int			_bluePawsTaken;
  int			_redPawsTaken;
  
};

#endif /* !GAME_HH_ */
