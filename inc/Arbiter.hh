//
// Arbiter.hh for  in /home/switi/Documents/ai-gomoku
//
// Made by Switi
// Login   <switi@epitech.net>
//
// Started on  Mon Nov  3 11:08:34 2014 Switi
// Last update Sun Jan  4 17:13:17 2015 Lazio
//

#ifndef		ARBITER_HH_
# define	ARBITER_HH_

# include <vector>
# include <sstream>
# include "Cell.hh"

class		Arbiter
{
public:
  Arbiter(const std::vector<Cell *> &, const bool &, const bool &);
  ~Arbiter();
  void				update();
  void				changeTurn();
  bool				isBlueTurn() const;
  void				checkVictory();
  void				addTake();
  bool				isOver() const;
  bool				blueWin() const;
  bool				isThisValid(int x, int y) const;
  std::string			getBluePawsTaken() const;
  std::string			getRedPawsTaken() const;
  bool				canBeTaken(int index) const;
  int				get3Align(int index, bool recurs) const;

private:
  const std::vector<Cell *>	&_map;
  const bool			&_pvp;
  const bool			&_help;
  bool				bluesTurn;
  int				_bluePawsTaken;
  int				_redPawsTaken;
  bool				_isOver;
  bool				_blueWin;
};

#endif		/* !ARBITER_HH_ */
