//
// Anim.hh for  in /home/switi/Documents/ai-gomoku
//
// Made by Switi
// Login   <switi@epitech.net>
//
// Started on  Mon Nov  3 10:52:11 2014 Switi
// Last update Mon Nov  3 11:07:33 2014 Switi
//

#ifndef		ANIM_HH_
# define	ANIM_HH_

# include "LoadTexture.hh"

class		Anim
{
public:
  Anim(sf::Texture &, sf::IntRect &, int &, int &, int &);
  ~Anim();

private:
  const sf::Texture	&_atlas;
};

#endif		/* !ANIM_HH_ */
