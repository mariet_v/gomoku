//
// LoadTexture.hh<2> for  in /home/switi/Documents/ai-gomoku
//
// Made by Switi
// Login   <switi@epitech.net>
//
// Started on  Tue Oct 21 18:37:30 2014 Switi
// Last update Fri Dec 26 19:20:44 2014 Lazio
//

#ifndef		LOADTEXTURE_HH_
# define	LOADTEXTURE_HH_

# include <iostream>
# include <SFML/Graphics.hpp>
# include <vector>

typedef enum {
  TITLE = 0,
  BLUE_WIN,
  RED_WIN,
  PVP,
  PVE
}		eTextureInterface;

typedef enum {
  BLUE_PAWN = 0,
  RED_PAWN
}		eTextureGame;

class		LoadTexture
{
public:
  LoadTexture();
  ~LoadTexture();
  const std::vector<sf::Texture>	&getTexturesInterface() const;
  const std::vector<sf::Texture>	&getTexturesGame() const;
private:
  bool				loadInterface();
  std::vector<sf::Texture>	texturesInterface;
  bool				loadGame();
  std::vector<sf::Texture>	texturesGame;
};

#endif		/* !LOADTEXTURE_HH_ */
