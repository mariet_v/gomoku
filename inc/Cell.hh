//
// Cell.hh for  in /home/switi/Documents/ai-gomoku
//
// Made by Switi
// Login   <switi@epitech.net>
//
// Started on  Sun Oct 26 20:33:33 2014 Switi
// Last update Thu Jan  1 17:02:31 2015 Lazio
//

#ifndef		CELL_HH_
# define	CELL_HH_

# include <iostream>

class		Cell
{
public:
  Cell();
  ~Cell();
  bool		isEmpty() const;
  bool		isRed() const;
  bool		isBlue() const;
  bool		putPawn(const bool&);
  void		setEmpty();

private:
  bool		_isRed;
  bool		_isBlue;
  bool		_isEmpty;

};

#endif		/* !CELL_HH_ */
