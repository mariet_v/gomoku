//
// AI.hh for AI in /home/Lazio/celem/ai-gomoku/inc
// 
// Made by Lazio
// Login   <Lazio@linux.site>
// 
// Started on Wed Dec 24 19:22:13 2014 Lazio
// Last update Sun Jan  4 15:48:43 2015 Lazio
//

#ifndef AI_HH_
# define AI_HH_
# define SPOT_NBR 100
# define NBR_SIMUL 100
# include <iostream>
# include <vector>
# include <SFML/Graphics.hpp>
# include <stdlib.h>
# include <time.h>
# include "Cell.hh"
# include "Spot.hh"
# include "Game.hh"

class AI
{
public:
  AI(std::vector<Cell *>  & map,int maxAITime);
  ~AI();
  void	play(bool _helpPlayer);
  void	simulate(int i);

private:
  std::vector<Cell *>   _map;
  std::vector<Spot *>	_spots;
  Spot *		_choosenSpot;
  sf::Clock		_clock;
  int			_maxAITime;
};

#endif /* !AI_HH_ */
