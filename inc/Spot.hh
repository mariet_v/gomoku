//
// Spot.hh for Spot in /home/Lazio/celem/ai-gomoku/inc
// 
// Made by Lazio
// Login   <Lazio@linux.site>
// 
// Started on Thu Jan  1 16:03:26 2015 Lazio
// Last update Thu Jan  1 16:45:24 2015 Lazio
//

#ifndef SPOT_HH_
# define SPOT_HH_
# include <iostream>

class Spot
{
public:
  Spot(int x, int y);
  ~Spot();
  int	getX(void) const;
  int	getY(void) const;
  int	getPCT(void) const;
  int	getNbrSimul(void) const;
  void	addSimul(bool win);

private:
  int		_x;
  int		_y;
  int		_nbrWin;
  int		_nbrLose;
};

#endif /* !SPOT_HH_ */
