##
## Makefile for  in /home/switi/Documents/ai-gomoku
##
## Made by Switi
## Login   <switi@epitech.net>
##
## Started on  Tue Oct 21 17:57:13 2014 Switi
## Last update Thu Jan  1 17:36:41 2015 Lazio
##

NAME	= 	gomoku

ODIR	=	./.obj/
SDIR	=	./src/
IDIR	=	./inc/

SRCS	= 	main.cpp \
		Cell.cpp \
		Gomoku.cpp \
		Arbiter.cpp \
		LoadTexture.cpp \
		AI.cpp \
		Spot.cpp \
		Game.cpp \

OBJS	=	$(addprefix $(ODIR), $(SRCS:.cpp=.o))
LIBS	+=	-lsfml-graphics
LIBS	+=	-lsfml-window
LIBS	+=	-lsfml-system
LIBS	+=	-lsfml-audio
CXXFLAGS +=	-W
CXXFLAGS +=	-Wall
CXXFLAGS +=	-Werror

$(ODIR)%.o:	$(SDIR)%.cpp
		$(CXX) $(CXXFLAGS) -o $@ -c $< -I$(IDIR)

$(NAME):	$(OBJS)
		$(CXX) $(CXXFLAGS) -L /usr/local/lib -I$(IDIR) $(OBJS) $(LIBS) -o $(NAME)

all:		$(NAME)

clean:
		$(RM) $(OBJS)

fclean: clean
		$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean all
