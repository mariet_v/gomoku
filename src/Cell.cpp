//
// Cell.cpp for  in /home/switi/Documents/ai-gomoku
//
// Made by Switi
// Login   <switi@epitech.net>
//
// Started on  Sun Oct 26 20:31:51 2014 Switi
// Last update Thu Dec 25 18:56:12 2014 Lazio
//

#include "Cell.hh"

Cell::Cell()
{
  this->_isEmpty = true;
  this->_isBlue = false;
  this->_isRed = false;
}

Cell::~Cell()
{

}

bool		Cell::putPawn(const bool &blue)
{
  if (blue)
    {
      if (this->_isEmpty == true && (!this->_isRed))
	this->_isBlue = true;
      else
	return (false);
      this->_isEmpty = false;
    }
  else
    {
      if (this->_isEmpty == true && (!this->_isBlue))
	this->_isRed = true;
      else
	return (false);
      this->_isEmpty = false;
    }
  return (true);
}

bool	Cell::isEmpty() const
{
  return (this->_isRed == false ? this->_isBlue == false ? true : false : false);
}

bool	Cell::isRed() const
{
  return (this->_isRed);
}

bool	Cell::isBlue() const
{
  return (this->_isBlue);
}

void	Cell::setEmpty()
{
  _isRed = false;
  _isBlue = false;
  _isEmpty = true;
}
