//
// Spot.cpp for Spot in /home/Lazio/celem/ai-gomoku/src
// 
// Made by Lazio
// Login   <Lazio@linux.site>
// 
// Started on Thu Jan  1 16:06:58 2015 Lazio
// Last update Sun Jan  4 15:55:49 2015 Lazio
//

#include "Spot.hh"

Spot::Spot(int x, int y)
{
  _x = x;
  _y = y;
  _nbrWin = 0;
  _nbrLose = 0;
}

Spot::~Spot()
{

}

int	Spot::getX(void) const
{
  return (_x);
}

int	Spot::getY(void) const
{
  return (_y);
}

int	Spot::getPCT(void) const
{
  if (_nbrWin > 0)
    return (_nbrWin * 100 / (_nbrLose + _nbrWin));
  return (0);
}

int	Spot::getNbrSimul(void) const
{
  return (_nbrWin + _nbrLose);
}

void	Spot::addSimul(bool win)
{
  if (win)
    _nbrWin++;
  else
    _nbrLose++;
}
