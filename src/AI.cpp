//
// Anim.cpp for  in /home/switi/Documents/ai-gomoku
//
// Made by Switi
// Login   <switi@epitech.net>
//
// Started on  Mon Nov  3 10:51:44 2014 Switi
// Last update Sun Jan  4 15:56:36 2015 Lazio
//

#include "AI.hh"

AI::AI(std::vector<Cell *>  & map, int maxAITime)
{
  _map = map;
  _maxAITime = maxAITime;
}

AI::~AI()
{

}

void	AI::play(bool _helpPlayer)
{
  int	i;
  int	bestpct = 0;
  
  _clock.restart();
  if (_helpPlayer)
    return;
  srand (time(NULL));
  for (i = 0; i < SPOT_NBR; i++)
    {
      _spots.push_back(new Spot(rand()%19, rand()%19));
    }
  for (i = 0; i < SPOT_NBR; i++)
    {
      if (_clock.getElapsedTime().asMilliseconds() >= _maxAITime)
	break;
      this->simulate(i);
      if (_spots[i]->getPCT() > bestpct)
	{
	  bestpct = _spots[i]->getPCT();
	  _choosenSpot = _spots[i];
	}
    }
  _map[_choosenSpot->getX() + _choosenSpot->getY()*19]->putPawn(false);
  _spots.clear();
}

void	AI::simulate(int spot)
{
  Game game(_map, _spots[spot]);
  game.simulate();
  for (int i = 0; i < NBR_SIMUL; i++)
    {
      _spots[spot]->addSimul(game.simulate());
      game.reset();
    }
}

