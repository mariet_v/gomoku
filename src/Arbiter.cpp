//
// Arbiter.cpp for  in /home/switi/Documents/ai-gomoku
//
// Made by Switi
// Login   <switi@epitech.net>
//
// Started on  Mon Nov  3 11:08:52 2014 Switi
// Last update Sun Jan  4 19:37:01 2015 Lazio
//

#include "Arbiter.hh"

Arbiter::Arbiter(const std::vector<Cell *> &map, const bool &pvp, const bool &help)
  : _map(map), _pvp(pvp), _help(help)
{
  this->bluesTurn = true;
  _bluePawsTaken = 0;
  _redPawsTaken = 0;
  _isOver = false;
  _blueWin = false;
}

Arbiter::~Arbiter()
{

}

void	Arbiter::update()
{
  int	a;
  int	red = 0;
  int	blue = 0;

  checkVictory();
  for (a = 0; a < 361; ++a)
    {
      if (this->_map[a]->isRed())
	++red;
      if (this->_map[a]->isBlue())
	++blue;
    }
}

bool	Arbiter::isBlueTurn() const
{
  return (this->bluesTurn ? true : false);
}

void	Arbiter::changeTurn()
{
  this->bluesTurn = !this->bluesTurn;
}

void	Arbiter::checkVictory()
{
  if (_bluePawsTaken >= 10)
    {
      _isOver = true;
      _blueWin = true;
    }
  else if (_redPawsTaken >= 10)
    _isOver = true;
  else
    {
      for (int a = 0; a < 361; ++a)
	{
	  if (canBeTaken(a))
	    continue;
	  if (this->_map[a]->isRed() && this->_map[a+1]->isRed() && this->_map[a+2]->isRed() && this->_map[a+3]->isRed() && this->_map[a+4]->isRed() && a/19 == (a+5)/19 && !canBeTaken(a + 1) && !canBeTaken(a + 2) && !canBeTaken(a + 3) && !canBeTaken(a + 4))
	    _isOver = true;
	  else if (this->_map[a]->isBlue() && this->_map[a+1]->isBlue() && this->_map[a+2]->isBlue() && this->_map[a+3]->isBlue() && this->_map[a+4]->isBlue() && a/19 == (a+5)/19 && !canBeTaken(a + 1) && !canBeTaken(a + 2) && !canBeTaken(a + 3) && !canBeTaken(a + 4))
	    {
	       _isOver = true;
	       _blueWin = true;
	    }
	  else if (a + 19*4 < 361 && this->_map[a]->isRed() && this->_map[a+19]->isRed() && this->_map[a+19*2]->isRed() && this->_map[a+19*3]->isRed() && this->_map[a+19*4]->isRed() && !canBeTaken(a + 19) && !canBeTaken(a + 2 * 19) && !canBeTaken(a + 3 * 19) && !canBeTaken(a + 4 * 19))
	    _isOver = true;
	  else if (a + 19*4 < 361 && this->_map[a]->isBlue() && this->_map[a+19]->isBlue() && this->_map[a+19*2]->isBlue() && this->_map[a+19*3]->isBlue() && this->_map[a+19*4]->isBlue() && !canBeTaken(a + 19) && !canBeTaken(a + 2 * 19) && !canBeTaken(a + 3 * 19) && !canBeTaken(a + 4 * 19))
	    {
	       _isOver = true;
	       _blueWin = true;
	    }
	  else if (a + 19*4 + 4 < 361 && this->_map[a]->isRed() && this->_map[a + 19 + 1]->isRed() && this->_map[a + 19*2 + 2]->isRed() && this->_map[a + 19*3 + 3]->isRed() && this->_map[a + 19*4 + 4]->isRed() && !canBeTaken(a + 19 + 1) && !canBeTaken(a + 2 * 19 + 2) && !canBeTaken(a + 3 * 19 + 3) && !canBeTaken(a + 4 * 19 + 4))
	    _isOver = true;
	  else if (a + 19*4 + 4 < 361 && this->_map[a]->isBlue() && this->_map[a + 19 + 1]->isBlue() && this->_map[a + 19*2 + 2]->isBlue() && this->_map[a + 19*3 + 3]->isBlue() && this->_map[a + 19*4 + 4]->isBlue() && !canBeTaken(a + 19 + 1) && !canBeTaken(a + 2 * 19 + 2) && !canBeTaken(a + 3 * 19 + 3) && !canBeTaken(a + 4 * 19 + 4))
	    {
	       _isOver = true;
	       _blueWin = true;
	    }
	  else if (a + 19*4 - 4 < 361 && this->_map[a]->isRed() && this->_map[a + 19 - 1]->isRed() && this->_map[a + 19*2 - 2]->isRed() && this->_map[a + 19*3 - 3]->isRed() && this->_map[a + 19*4 - 4]->isRed() && !canBeTaken(a + 19 - 1) && !canBeTaken(a + 2 * 19 - 2) && !canBeTaken(a + 3 * 19 - 3) && !canBeTaken(a + 4 * 19 - 4))
	    _isOver = true;
	  else if (a + 19*4 - 4 < 361 && this->_map[a]->isBlue() && this->_map[a + 19 - 1]->isBlue() && this->_map[a + 19*2 - 2]->isBlue() && this->_map[a + 19*3 - 3]->isBlue() && this->_map[a + 19*4 - 4]->isBlue() && !canBeTaken(a + 19 - 1) && !canBeTaken(a + 2 * 19 - 2) && !canBeTaken(a + 3 * 19 - 3) && !canBeTaken(a + 4 * 19 - 4))
	    {
	       _isOver = true;
	       _blueWin = true;
	    }
	}
    }
}

void	Arbiter::addTake(void)
{
  if (bluesTurn)
    _bluePawsTaken += 2 ;
  else
    _redPawsTaken += 2;
}

bool	Arbiter::isOver(void) const
{
  return (_isOver);
}

bool	Arbiter::blueWin(void) const
{
  return (_blueWin);
}

std::string	Arbiter::getBluePawsTaken() const
{
  std::ostringstream s;
  s << _bluePawsTaken << std::flush;
  return(s.str());
}

std::string	Arbiter::getRedPawsTaken() const
{
  std::ostringstream s;
  s << _redPawsTaken << std::flush;
  return(s.str());
}

bool	Arbiter::isThisValid(int x, int y) const
{
  if (!_map[x + y*19]->isEmpty())
    {
      std::cout << "You can't play here, there is an another pawn !" << std::endl;
      return (false);
    }
  _map[x + y * 19]->putPawn(bluesTurn);
  if (get3Align(x + y * 19, true) >= 2)
    {
      _map[x + y * 19]->setEmpty();
      std::cout << "You can't play here, we play with the Double Three Rule" << std::endl;
      return (false);
    }
  _map[x + y * 19]->setEmpty();
  return (true);
}

int	Arbiter::get3Align(int index, bool recurs) const
{
  int	free3paws;
  int	paws2;
  int	paws3;

  free3paws = 0;
  paws2 = 0;
  paws3 = 0;
  if (index > 0 && index%19 < 17 && _map[index + 1]->isBlue() == bluesTurn && _map[index + 2]->isBlue() == bluesTurn && _map[index + 3]->isEmpty() && !_map[index + 1]->isEmpty() && !_map[index + 2]->isEmpty() && _map[index - 1]->isEmpty())
    {
      paws2 = index + 1;
      paws3 = index + 2;
      free3paws++;
    }
  if (index > 1 && index%19 < 18 && _map[index + 1]->isBlue() == bluesTurn && _map[index - 1]->isBlue() == bluesTurn && _map[index + 2]->isEmpty() && _map[index - 2]->isEmpty() && !_map[index + 1]->isEmpty() && !_map[index - 1]->isEmpty())
    {
      paws2 = index - 1;
      paws3 = index + 1;
      free3paws++;
    }
  if (index > 2 && index%19 < 19 && _map[index - 2]->isBlue() == bluesTurn && _map[index - 1]->isBlue() == bluesTurn && _map[index - 3]->isEmpty() && _map[index + 1]->isEmpty() && !_map[index - 1]->isEmpty() && !_map[index - 2]->isEmpty())
    {
      paws2 = index - 1;
      paws3 = index - 2;
      free3paws++;
    }
  if (index/19 > 0 && index/19 < 17 && _map[index + 19]->isBlue() == bluesTurn && _map[index + 2*19]->isBlue() == bluesTurn && _map[index + 3 * 19]->isEmpty() && !_map[index + 19]->isEmpty() && !_map[index + 2*19]->isEmpty() && _map[index - 19]->isEmpty())
    {
      paws2 = index + 19;
      paws3 = index + 2*19;
      free3paws++;
    }
  if (index/19 > 1 && index/19 < 18 && _map[index + 19]->isBlue() == bluesTurn && _map[index - 19]->isBlue() == bluesTurn && _map[index + 2*19]->isEmpty() && _map[index - 2*19]->isEmpty() && !_map[index + 19]->isEmpty() && !_map[index - 19]->isEmpty())
    {
      paws2 = index - 19;
      paws3 = index + 19;
      free3paws++;
    }
  if (index/19 > 2 && index/19 < 19 && _map[index - 2*19]->isBlue() == bluesTurn && _map[index - 19]->isBlue() == bluesTurn && _map[index - 3*19]->isEmpty() && _map[index + 19]->isEmpty() && !_map[index - 19]->isEmpty() && !_map[index - 2*19]->isEmpty())
    {
      paws2 = index - 19;
      paws3 = index - 2*19;
      free3paws++;
    }
  if (index > 0 && index%19 < 17 && index/19 > 2 && index/19 < 19 && _map[index - 19 + 1]->isBlue() == bluesTurn && _map[index - 19*2 + 2]->isBlue() == bluesTurn && _map[index - 19*3 + 3]->isEmpty() && !_map[index - 19 + 1]->isEmpty() && !_map[index - 19*2 + 2]->isEmpty() && _map[index - 19 - 1]->isEmpty())
    {
      paws2 = index + 1 - 19;
      paws3 = index + 2 - 19*2;
      free3paws++;
    }
  if (index > 1 && index%19 < 18 && index/19 > 1 && index/19 < 18 && _map[index - 19 + 1]->isBlue() == bluesTurn && _map[index + 19 - 1]->isBlue() == bluesTurn && _map[index - 19*2 + 2]->isEmpty() && _map[index + 19*2 - 2]->isEmpty() && !_map[index - 19 + 1]->isEmpty() && !_map[index +19 - 1]->isEmpty())
    {
      paws2 = index + 19 - 1;
      paws3 = index -19 + 1;
      free3paws++;
    }
  if (index > 2 && index%19 < 19 && index/19 > 0 && index/19 < 17 && _map[index + 19*2 - 2]->isBlue() == bluesTurn && _map[index + 19 - 1]->isBlue() == bluesTurn && _map[index + 19*3 - 3]->isEmpty() && _map[index - 19 + 1]->isEmpty() && !_map[index + 19 - 1]->isEmpty() && !_map[index + 19*2 - 2]->isEmpty())
    {
      paws2 = index + 19 - 1;
      paws3 = index + 19*2 - 2;
      free3paws++;
    }
  if (index > 0 && index%19 < 17 && index/19 > 0 && index/19 < 17 && _map[index + 19 + 1]->isBlue() == bluesTurn && _map[index + 19*2 + 2]->isBlue() == bluesTurn && _map[index + 19*3 + 3]->isEmpty() && !_map[index + 19 + 1]->isEmpty() && !_map[index + 19*2 + 2]->isEmpty() && _map[index - 19 - 1]->isEmpty())
    {
      paws2 = index + 19 + 1;
      paws3 = index + 19*2 + 2;
      free3paws++;
    }
  if (index > 1 && index%19 < 18 && index/19 > 1 && index/19 < 18 && _map[index + 19 + 1]->isBlue() == bluesTurn && _map[index - 19 - 1]->isBlue() == bluesTurn && _map[index + 19*2 + 2]->isEmpty() && _map[index - 19*2- 2]->isEmpty() && !_map[index + 19 + 1]->isEmpty() && !_map[index -19 - 1]->isEmpty())
    {
      paws2 = index - 19 - 1;
      paws3 = index + 19 + 1;
      free3paws++;
    }
  if (index > 2 && index%19 < 19 && index/ 19 > 2 && index/19 < 19 && _map[index - 19*2 - 2]->isBlue() == bluesTurn && _map[index - 19 - 1]->isBlue() == bluesTurn && _map[index - 19*3 - 3]->isEmpty() && _map[index + 19 + 1]->isEmpty() && !_map[index - 19 - 1]->isEmpty() && !_map[index - 19*2 - 2]->isEmpty())
    {
      paws2 = index - 19 - 1;
      paws3 = index - 19*2 - 2;
      free3paws++;
    }
  if (index > 0 && index%19 < 16 && _map[index + 2]->isBlue() == bluesTurn && _map[index + 3]->isBlue() == bluesTurn && _map[index + 4]->isEmpty() && _map[index + 1]->isEmpty() && !_map[index + 2]->isEmpty() && !_map[index + 3]->isEmpty() && _map[index - 1]->isEmpty() && _map[index + 4]->isEmpty())
    {
      paws2 = index + 2;
      paws3 = index + 3;
      free3paws++;
    }
  if (index > 2 && index%19 < 18 && _map[index - 2]->isBlue() == bluesTurn && _map[index + 1]->isBlue() == bluesTurn && _map[index + 2]->isEmpty() && _map[index - 1]->isEmpty() && !_map[index + 1]->isEmpty() && !_map[index - 2]->isEmpty() && _map[index - 1]->isEmpty() && _map[index - 3]->isEmpty())
    {
      paws2 = index + 1;
      paws3 = index - 2;
      free3paws++;
    }
  if (index > 3 && index%19 < 19 && _map[index - 3]->isBlue() == bluesTurn && _map[index - 1]->isBlue() == bluesTurn && _map[index + 1]->isEmpty() && _map[index - 2]->isEmpty() && !_map[index - 1]->isEmpty() && !_map[index - 3]->isEmpty() && _map[index - 4]->isEmpty())
    {
      paws2 = index - 1;
      paws3 = index - 3;
      free3paws++;
    }
  if (index > 0 && index%19 < 16 && _map[index + 1]->isBlue() == bluesTurn && _map[index + 3]->isBlue() == bluesTurn && _map[index - 1]->isEmpty() && _map[index + 2]->isEmpty() && _map[index + 4]->isEmpty() && !_map[index + 1]->isEmpty() && !_map[index + 3]->isEmpty())
    {
      paws2 = index + 1;
      paws3 = index + 3;
      free3paws++;
    }
  if (index > 1 && index%19 < 17 && _map[index - 1]->isBlue() == bluesTurn && _map[index + 2]->isBlue() == bluesTurn && _map[index - 2]->isEmpty() && _map[index + 1]->isEmpty() && _map[index + 3]->isEmpty() && !_map[index - 1]->isEmpty() && !_map[index + 2]->isEmpty())
    {
      paws2 = index - 1;
      paws3 = index + 2;
      free3paws++;
    }
  if (index > 3 && index%19 < 19 && _map[index - 3]->isBlue() == bluesTurn && _map[index - 2]->isBlue() == bluesTurn && _map[index - 4]->isEmpty() && _map[index - 1]->isEmpty() && _map[index + 1]->isEmpty() && !_map[index - 3]->isEmpty() && !_map[index - 2]->isEmpty())
    {
      paws2 = index - 3;
      paws3 = index - 2;
      free3paws++;
    }
  if (index/19 > 0 && index%19 < 16 && _map[index + 2*19]->isBlue() == bluesTurn && _map[index + 3*19]->isBlue() == bluesTurn && _map[index - 19]->isEmpty() && _map[index + 19]->isEmpty() && _map[index + 4*19]->isEmpty() && !_map[index + 2*19]->isEmpty() && !_map[index + 3*19]->isEmpty())
    {
      paws2 = index + 2*19;
      paws3 = index + 3*19;
      free3paws++;
    }
  if (index/19 > 2 && index%19 < 18 && _map[index - 2*19]->isBlue() == bluesTurn && _map[index + 19]->isBlue() == bluesTurn && _map[index - 3*19]->isEmpty() && _map[index - 19]->isEmpty() && _map[index + 2*19]->isEmpty() && !_map[index - 2*19]->isEmpty() && !_map[index + 19]->isEmpty())
    {
      paws2 = index - 2*19;
      paws3 = index + 19;
      free3paws++;
    }
  if (index/19 > 3 && index%19 < 19 && _map[index - 3*19]->isBlue() == bluesTurn && _map[index - 19]->isBlue() == bluesTurn && _map[index - 4*19]->isEmpty() && _map[index - 2*19]->isEmpty() && _map[index + 19]->isEmpty() && !_map[index - 3*19]->isEmpty() && !_map[index - 19]->isEmpty())
    {
      paws2 = index - 3*19;
      paws3 = index - 19;
      free3paws++;
    }
  if (index/19 > 0 && index%19 < 16 && _map[index + 19]->isBlue() == bluesTurn && _map[index + 3*19]->isBlue() == bluesTurn && _map[index - 19]->isEmpty() && _map[index + 2*19]->isEmpty() && _map[index + 4*19]->isEmpty() && !_map[index + 19]->isEmpty() && !_map[index + 3*19]->isEmpty())
    {
      paws2 = index + 19;
      paws3 = index + 3*19;
      free3paws++;
    }
  if (index/19 > 1 && index%19 < 17 && _map[index - 19]->isBlue() == bluesTurn && _map[index + 2*19]->isBlue() == bluesTurn && _map[index - 2*19]->isEmpty() && _map[index + 19]->isEmpty() && _map[index + 3*19]->isEmpty() && !_map[index - 19]->isEmpty() && !_map[index + 2*19]->isEmpty())
    {
      paws2 = index - 19;
      paws3 = index + 2*19;
      free3paws++;
    }
  if (index/19 > 3 && index%19 < 19 && _map[index - 3*19]->isBlue() == bluesTurn && _map[index - 2*19]->isBlue() == bluesTurn && _map[index - 4*19]->isEmpty() && _map[index - 19]->isEmpty() && _map[index + 19]->isEmpty() && !_map[index - 3*19]->isEmpty() && !_map[index - 2*19]->isEmpty())
    {
      paws2 = index - 3*19;
      paws3 = index - 2*19;
      free3paws++;
    }
  if (index > 0 && index%19 < 16 && index/19 > 3 && index/19 < 19 && _map[index - 2*19 + 2]->isBlue() == bluesTurn && _map[index - 3*19 + 3]->isBlue() == bluesTurn && _map[index - 4*19 + 4]->isEmpty() && _map[index - 19 + 1]->isEmpty() && !_map[index - 2*19 + 2]->isEmpty() && !_map[index - 3*19 + 3]->isEmpty() && _map[index + 19 - 1]->isEmpty() && _map[index - 4*19 + 4]->isEmpty())
    {
      paws2 = index - 2*19 + 2;
      paws3 = index - 3*19 + 3;
      free3paws++;
    }
  if (index > 2 && index%19 < 18 && index/19 > 1 && index/19 < 17 && _map[index + 2*19 - 2]->isBlue() == bluesTurn && _map[index - 19 + 1]->isBlue() == bluesTurn && _map[index - 2*19 + 2]->isEmpty() && _map[index + 19 - 1]->isEmpty() && !_map[index - 19 + 1]->isEmpty() && !_map[index + 2*19 - 2]->isEmpty() && _map[index + 19 - 1]->isEmpty() && _map[index + 3*19 - 3]->isEmpty())
    {
      paws2 = index - 19 + 1;
      paws3 = index + 2*19 - 2;
      free3paws++;
    }
  if (index > 3 && index%19 < 19 && index/19 > 0 && index/19 < 16 && _map[index + 19*3 - 3]->isBlue() == bluesTurn && _map[index + 19 - 1]->isBlue() == bluesTurn && _map[index - 19 + 1]->isEmpty() && _map[index + 2*19 - 2]->isEmpty() && !_map[index + 19 - 1]->isEmpty() && !_map[index + 3*19 - 3]->isEmpty() && _map[index + 4*19 - 4]->isEmpty())
    {
      paws2 = index + 19 - 1;
      paws3 = index + 3*19 - 3;
      free3paws++;
    }
  if (index > 0 && index%19 < 16 && index/19 > 3 && index/19 < 19 && _map[index - 19 + 1]->isBlue() == bluesTurn && _map[index - 3*19 + 3]->isBlue() == bluesTurn && _map[index + 19 - 1]->isEmpty() && _map[index -2*19 + 2]->isEmpty() && _map[index - 4*19 + 4]->isEmpty() && !_map[index - 19 + 1]->isEmpty() && !_map[index - 3*19 + 3]->isEmpty())
    {
      paws2 = index - 19 + 1;
      paws3 = index - 3*19 + 3;
      free3paws++;
    }
  if (index > 1 && index%19 < 17 && index/19 > 2 && index/19 < 18 && _map[index + 19 - 1]->isBlue() == bluesTurn && _map[index - 2*19 + 2]->isBlue() == bluesTurn && _map[index + 2*19 - 2]->isEmpty() && _map[index - 19 + 1]->isEmpty() && _map[index - 3*19 + 3]->isEmpty() && !_map[index + 19 - 1]->isEmpty() && !_map[index - 2*19 + 2]->isEmpty())
    {
      paws2 = index + 19 - 1;
      paws3 = index - 2*19 + 2;
      free3paws++;
    }
  if (index > 3 && index%19 < 19 && index/19 > 0 && index/19 < 16 && _map[index + 3*19 - 3]->isBlue() == bluesTurn && _map[index +2*19 - 2]->isBlue() == bluesTurn && _map[index + 4*19 - 4]->isEmpty() && _map[index + 19 - 1]->isEmpty() && _map[index - 19 + 1]->isEmpty() && !_map[index + 3*19 - 3]->isEmpty() && !_map[index + 2*19 - 2]->isEmpty())
    {
      paws2 = index + 3*19 - 3;
      paws3 = index + 3*19 - 2;
      free3paws++;
    }
  if (index > 0 && index%19 < 16 && index/19 > 0 && index/19 < 16 && _map[index + 2*19 + 2]->isBlue() == bluesTurn && _map[index + 3*19 + 3]->isBlue() == bluesTurn && _map[index + 4*19 + 4]->isEmpty() && _map[index + 19 + 1]->isEmpty() && !_map[index + 2*19 + 2]->isEmpty() && !_map[index + 3*19 + 3]->isEmpty() && _map[index - 19 - 1]->isEmpty() && _map[index + 4*19 + 4]->isEmpty())
    {
      paws2 = index + 2*19 + 2;
      paws3 = index + 3*19 + 3;
      free3paws++;
    }
  if (index > 2 && index%19 < 18 && index/19 > 2 && index/19 < 18 && _map[index - 2*19 - 2]->isBlue() == bluesTurn && _map[index + 19 + 1]->isBlue() == bluesTurn && _map[index + 2*19 + 2]->isEmpty() && _map[index - 19 - 1]->isEmpty() && !_map[index + 19 + 1]->isEmpty() && !_map[index - 2*19 - 2]->isEmpty() && _map[index - 19 - 1]->isEmpty() && _map[index - 3*19 - 3]->isEmpty())
    {
      paws2 = index + 19 + 1;
      paws3 = index - 2*19 - 2;
      free3paws++;
    }
  if (index > 3 && index%19 < 19 && index/19 > 3 && index/19 < 19 && _map[index - 3*19 - 3]->isBlue() == bluesTurn && _map[index - 19 - 1]->isBlue() == bluesTurn && _map[index + 19 + 1]->isEmpty() && _map[index - 2*19 - 2]->isEmpty() && !_map[index - 19 - 1]->isEmpty() && !_map[index - 3*19 - 3]->isEmpty() && _map[index - 4*19 - 4]->isEmpty())
    {
      paws2 = index - 19 - 1;
      paws3 = index - 3*19 - 3;
      free3paws++;
    }
  if (index > 0 && index%19 < 16 && index/19 > 0 && index/19 < 16 && _map[index + 19 + 1]->isBlue() == bluesTurn && _map[index + 3*19 + 3]->isBlue() == bluesTurn && _map[index - 19 - 1]->isEmpty() && _map[index + 2*19 + 2]->isEmpty() && _map[index + 4*19 + 4]->isEmpty() && !_map[index + 19 + 1]->isEmpty() && !_map[index + 3*19 + 3]->isEmpty())
    {
      paws2 = index + 19 + 1;
      paws3 = index + 3*19 + 3;
      free3paws++;
    }
  if (index > 1 && index%19 < 17 && index/19 > 1 && index/19 < 17 && _map[index - 19 - 1]->isBlue() == bluesTurn && _map[index + 2*19 + 2]->isBlue() == bluesTurn && _map[index - 2*19 - 2]->isEmpty() && _map[index + 19 + 1]->isEmpty() && _map[index + 3*19 + 3]->isEmpty() && !_map[index - 19 - 1]->isEmpty() && !_map[index + 2*19 + 2]->isEmpty())
    {
      paws2 = index - 19 - 1;
      paws3 = index + 2*19 + 2;
      free3paws++;
    }
  if (index > 3 && index%19 < 19 && index/19 > 3 && index/19 < 19 && _map[index - 3*19 - 3]->isBlue() == bluesTurn && _map[index -2*19 - 2]->isBlue() == bluesTurn && _map[index - 4*19 - 4]->isEmpty() && _map[index - 19 - 1]->isEmpty() && _map[index + 19 + 1]->isEmpty() && !_map[index -3*19 - 3]->isEmpty() && !_map[index -2*19 - 2]->isEmpty())
    {
      paws2 = index - 3*19 - 3;
      paws3 = index - 2*19 - 2;
      free3paws++;
    }
  if (paws2 > 0 && paws3 > 0 && recurs)
    {
      free3paws += get3Align(paws2, false) - 1;
      free3paws += get3Align(paws3, false) - 1;
    }
  return (free3paws);
}

bool	Arbiter::canBeTaken(int index) const
{
  if (index > 0 && index%19 < 18 && _map[index]->isBlue() != _map[index - 1]->isBlue() && _map[index]->isBlue() == _map[index + 1]->isBlue() && !_map[index]->isEmpty() && !_map[index - 1]->isEmpty() && !_map[index + 1]->isEmpty() && _map[index + 2]->isEmpty())
    return (true);
  if (index > 1 && index%19 < 19 && _map[index]->isBlue() != _map[index - 2]->isBlue() && _map[index]->isBlue() == _map[index - 1]->isBlue() && !_map[index]->isEmpty() && !_map[index - 1]->isEmpty() && !_map[index - 2]->isEmpty() && _map[index + 1]->isEmpty())
    return (true);
  if (index > 0 && index%19 < 17 && _map[index]->isBlue() == _map[index + 1]->isBlue() && _map[index]->isBlue() != _map[index + 2]->isBlue() && _map[index - 1]->isEmpty() && !_map[index]->isEmpty() && !_map[index + 1]->isEmpty() && !_map[index + 2]->isEmpty())
    return (true);
  if (index > 1 && index%19 < 18 && _map[index]->isBlue() == _map[index - 1]->isBlue() && _map[index]->isBlue() != _map[index + 1]->isBlue() && _map[index - 2]->isEmpty() && !_map[index]->isEmpty() && !_map[index + 1]->isEmpty() && !_map[index - 1]->isEmpty())
    return (true);
  if (index/19 > 0 && index/19 < 18 && _map[index]->isBlue() == _map[index + 19]->isBlue() && _map[index]->isBlue() != _map[index - 19]->isBlue() && _map[index + 19*2]->isEmpty() && !_map[index]->isEmpty() && !_map[index + 19]->isEmpty() && !_map[index - 19]->isEmpty())
    return (true);
  if (index/19 > 1 && index/19 < 19 && _map[index]->isBlue() == _map[index - 19]->isBlue() && _map[index]->isBlue() != _map[index - 19*2]->isBlue() && _map[index + 19]->isEmpty() && !_map[index]->isEmpty() && !_map[index - 19]->isEmpty() && !_map[index - 19*2]->isEmpty())
    return (true);
  if (index/19 > 0 && index/19 < 18 && _map[index]->isBlue() == _map[index + 19]->isBlue() && _map[index]->isBlue() != _map[index + 19*2]->isBlue() && _map[index - 19]->isEmpty() && !_map[index]->isEmpty() && !_map[index + 19]->isEmpty() && !_map[index + 19*2]->isEmpty())
    return (true);
  if (index/19 > 1 && index/19 < 19 && _map[index]->isBlue() == _map[index - 19]->isBlue() && _map[index]->isBlue() != _map[index + 19]->isBlue() && _map[index - 19*2]->isEmpty() && !_map[index]->isEmpty() && !_map[index - 19]->isEmpty() && !_map[index + 19]->isEmpty())
    return (true);
  return (false);
}
