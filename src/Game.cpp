//
// Game.cpp for Game in /home/Lazio/celem/ai-gomoku/src
// 
// Made by Lazio
// Login   <Lazio@linux.site>
// 
// Started on Thu Jan  1 17:22:04 2015 Lazio
// Last update Sun Jan  4 15:52:08 2015 Lazio
//

# include "Game.hh"

Game::Game(std::vector<Cell *> &map, Spot * spot)
{
  _cellmap = map;
  _myTurn = false;
  _aiWin = false;
  _spot = spot;
  _bluePawsTaken = 0;
  _redPawsTaken = 0;
  this->reset();
}

Game::~Game()
{

}

bool	Game::simulate(void)
{
  int	x;
  int	y;

  while (!this->checkWin())
    {
      x = rand()%19;
      y = rand()%19;
      if (this->isThisValid(x, y))
	{
	  if (_myTurn)
	    _map[y][x] = 1;
	  else
	    _map[y][x] = 2;
	  this->take(x,y);
	  _myTurn = !_myTurn;
	}
    }
  return (_aiWin);
}

void	Game::reset(void)
{
  for (int i = 0; i < 19; i++)
    {
      for (int j = 0; j < 19; j++)
        {
          if (_cellmap[j + i*19]->isRed())
            _map[i][j] = 1;
          else if (_cellmap[j + i*19]->isBlue())
            _map[i][j] = 2;
          else
            _map[i][j] = 0;
        }
    }
  _map[_spot->getX()][ _spot->getY()] = 1;
}

bool	Game::checkWin(void)
{
  if (_redPawsTaken >= 10)
    {
      _aiWin = true;
      return (true);
    }
  else if (_bluePawsTaken >= 10)
    {
      _aiWin = false;
      return (true);
    }
  for (int i = 0; i < 19; i++)
    {
      for (int j = 0; j < 19; j++)
	{
	  if (j < 14 && _map[i][j] > 0 && _map[i][j + 1] == _map[i][j] && _map[i][j + 2] == _map[i][j] && _map[i][j + 3] == _map[i][j] && _map[i][j + 4] == _map[i][j])
	    {
	      _map[i][j] == 1 ? _aiWin = true : _aiWin = false;
	      return (true);
	    }
	  else if (i < 14 && _map[i][j] > 0 && _map[i + 1][j] == _map[i][j] && _map[i + 2][j] == _map[i][j] && _map[i + 3][j] == _map[i][j] && _map[i + 4][j] == _map[i][j])
	    {
	      _map[i][j] == 1 ? _aiWin = true : _aiWin = false;
	      return (true);
	    }
	}
    }
  return (false);
}

bool	Game::isThisValid(int x, int y)
{
  if (_map[y][x] == 0)
    return (true);
  return (false);
}

void	Game::take(int x, int y)
{
  if (x < 16 && _map[y][x] != 0 && _map[y][x] == _map[y][x + 3] && _map[y][x] != _map[y][x + 1] && _map[y][x + 1] != 0 && _map[y][x] != _map[y][x + 2] && _map[y][x + 2] != 0)
    {
      _map[y][x + 1] = 0;
      _map[y][x + 2] = 0;
      if (_myTurn)
	_redPawsTaken += 2;
      else
	_bluePawsTaken += 2;

    }
  if (x > 3 && _map[y][x] != 0 && _map[y][x] == _map[y][x - 3] && _map[y][x] != _map[y][x - 1] && _map[y][x - 1] != 0 && _map[y][x] != _map[y][x - 2] && _map[y][x - 2] != 0)
    {
      _map[y][x - 1] = 0;
      _map[y][x - 2] = 0;
      if (_myTurn)
	_redPawsTaken += 2;
      else
	_bluePawsTaken += 2;
    }
    if (y < 16 && _map[y][x] != 0 && _map[y][x] == _map[y + 3][x] && _map[y][x] != _map[y + 1][x] && _map[y + 1][x] != 0 && _map[y][x] != _map[y + 2][x] && _map[y + 2][x] != 0)
    {
      _map[y + 1][x] = 0;
      _map[y + 1][x] = 0;
      if (_myTurn)
	_redPawsTaken += 2;
      else
	_bluePawsTaken += 2;
    }
    if (y > 3 && _map[y][x] != 0 && _map[y][x] != 0 && _map[y][x] == _map[y - 3][x] && _map[y][x] != _map[y - 1][x] && _map[y - 1][x] != 0 && _map[y][x] != _map[y - 2][x] && _map[y - 2][x] != 0)
    {
      _map[y - 1][x] = 0;
      _map[y - 1][x] = 0;
      if (_myTurn)
	_redPawsTaken += 2;
      else
	_bluePawsTaken += 2;
    }
}
