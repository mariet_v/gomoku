//
// main.cpp for  in /home/switi/Documents/ai-gomoku
//
// Made by Switi
// Login   <switi@epitech.net>
//
// Started on  Tue Oct 21 18:02:52 2014 Switi
// Last update Sun Jan  4 15:28:57 2015 Lazio
//

#include "Gomoku.hh"

int		main(int ac, char **av)
{
  try
    {
      Gomoku	G(ac, av);
      G.runInterface();
    }
  catch (const std::string &e)
    {
      std::cerr << e << std::endl;
      return (-1);
    }
  (void) ac;
  (void) av;
  return (0);
}
