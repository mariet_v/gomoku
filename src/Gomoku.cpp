//
// Gomoku.cpp for Gomoku in /home/leniglo/Projects/ai-gomoku
//
// Made by Lefrant Guillaume
// Login   <leniglo@epitech.net>
//
// Started on  Tue Oct 21 18:19:04 2014 Lefrant Guillaume
// Last update Sun Jan  4 19:58:17 2015 Lazio
//

#include "Gomoku.hh"

Gomoku::Gomoku(int ac, char **av)
{
  this->_ai = NULL;
  this->_window.create(sf::VideoMode(1024, 768), "Gomoku", sf::Style::Titlebar);
  this->_window.setKeyRepeatEnabled(false);
  this->_window.setPosition(sf::Vector2i((sf::VideoMode::getDesktopMode().width / 2) - 512,
    (sf::VideoMode::getDesktopMode().height / 2) - 384));
  try
  {
    this->_textures = new LoadTexture();
    if (!_font.loadFromFile("media/arial.ttf"))
      {
	throw (std::string("Unable to load fonts."));
      }
    _redText.setFont(_font);
    _redText.setCharacterSize(24);
    _redText.setColor(sf::Color::Red);
    _redText.setStyle(sf::Text::Bold);
    _redText.setPosition(790, 400);
    _blueText.setFont(_font);
    _blueText.setCharacterSize(24);
    _blueText.setColor(sf::Color::Blue);
    _blueText.setStyle(sf::Text::Bold);
    _blueText.setPosition(790, 40);
    _yourTurnBlue.setFont(_font);
    _yourTurnBlue.setCharacterSize(24);
    _yourTurnBlue.setStyle(sf::Text::Bold);
    _yourTurnBlue.setString("Your turn !");
    _yourTurnBlue.setPosition(840, 100);
    _yourTurnBlue.setColor(sf::Color::Blue);
    _yourTurnRed.setFont(_font);
    _yourTurnRed.setCharacterSize(24);
    _yourTurnRed.setStyle(sf::Text::Bold);
    _yourTurnRed.setPosition(840, 484);
    _yourTurnRed.setString("Your turn !");
    _yourTurnRed.setColor(sf::Color::Red);
  }
  catch (const std::string &e)
  {
    std::cerr << e << std::endl;
    throw (std::string("Error while creating the game."));
  }
  this->_blueTurn = true;
  this->_exit = false;
  this->_pvp = true;
  this->_maxAITime = 0;
  if (ac > 1)
    {
      std::stringstream ss;
      ss << av[1];
      ss >> _maxAITime;
      if (_maxAITime != 10 && _maxAITime != 20 && _maxAITime != 50)
	_maxAITime = 0;
    }
}

Gomoku::~Gomoku()
{

}

bool			Gomoku::gestEventInterface()
{
  sf::Event event;

  while (this->_window.pollEvent(event))
  {
    switch (event.type)
      {
      case sf::Event::Closed:
	this->_window.close();
	break;
      case sf::Event::KeyPressed:
	switch ((int)event.key.code)
	  {
	  case sf::Keyboard::Escape:
	    this->_window.close();
	    exit(0);
	    break;
	  case sf::Keyboard::Return:
	    this->runGame();
	    this->_exit = false;
	    break;
	  default:
	    break;
	  }
	break;
     
    case sf::Event::MouseButtonPressed:
      if (event.mouseButton.button == sf::Mouse::Left)
	{
	  if (event.mouseButton.x > 285 && event.mouseButton.x < 739 && event.mouseButton.y > 204 && event.mouseButton.y < 308)
	    {
	      this->_pvp = true;
	      this->runGame();
	      this->_exit = false;
	    }
	  else if (event.mouseButton.x > 285 && event.mouseButton.x < 739 && event.mouseButton.y > 332 && event.mouseButton.y < 436)
	    {
	      this->_pvp = false;
	      this->runGame();
	      this->_exit = false;
	    }
	}
      default:
      break;
      }
  }
  return (true);
}

bool			Gomoku::displayInterface()
{
  this->displayTextureInterface(TITLE, 1024/2-454/2, 768/8-104/2);
  this->displayTextureInterface(PVP, 1024/2-454/2, 768/3-104/2);
  this->displayTextureInterface(PVE, 1024/2-454/2, 768/2-104/2);
  return (true);
}

bool			Gomoku::displayTextureInterface(eTextureInterface tex, int x, int y)
{
  sf::Sprite sp(this->_textures->getTexturesInterface()[tex]);
  sp.move(x, y);
  this->_window.draw(sp);
  return (true);
}

bool			Gomoku::gestEventGame()
{
  sf::Event event;

  while (this->_window.pollEvent(event))
    {
      switch (event.type)
	{
	case sf::Event::Closed:
	  this->_window.close();
	  break;
	case sf::Event::KeyPressed:
	  switch ((int) event.key.code)
	    {
	    case sf::Keyboard::Escape:
	      this->_exit = true;
	      break;
	    default:
	      break;
	    }
	  break;
	case sf::Event::MouseButtonPressed:
	  if (event.mouseButton.button == sf::Mouse::Left && !_arb->isOver())
	    {
	      int x = event.mouseButton.x / 40;
	      int y = event.mouseButton.y / 40;
	      int index = y*19+x%19;
	      if (this->_arb->isThisValid(x, y))
		{
		  this->_map[index]->putPawn(this->_arb->isBlueTurn());
		  this->take(index);
		  this->_arb->changeTurn();
		  this->_arb->update();
		}
	    }
	default:
	  break;
	}
    }
  return (true);
}

bool			Gomoku::displayGame()
{
  sf::RectangleShape	canvas(sf::Vector2f(760, 760));
  sf::RectangleShape	player1(sf::Vector2f(252, 378));
  sf::RectangleShape	player2(sf::Vector2f(252, 378));
  sf::RectangleShape	borderV(sf::Vector2f(4, 768));
  sf::RectangleShape	borderH(sf::Vector2f(1080, 4));
  int			i = -1;
  int			j;
  float			r, b;
 
  canvas.setPosition(4, 4);
  player1.setPosition(768, 4);
  player2.setPosition(768, 386);
  borderH.setPosition(0,0);
  borderV.setPosition(0,0);

  canvas.setFillColor(sf::Color(20, 20, 20));
  player1.setFillColor(sf::Color(103, 103, 103));
  player2.setFillColor(sf::Color(103, 103, 103));
  player1.setOutlineThickness(-4);
  player2.setOutlineThickness(-4);
  player1.setOutlineColor(sf::Color(80, 80, 255));
  player2.setOutlineColor(sf::Color(255, 80, 80));
  borderH.setFillColor(sf::Color(206, 206, 206));
  borderV.setFillColor(sf::Color(206, 206, 206));
  this->_window.draw(canvas);
  r = 1;
  b = 1;
  while (++i < 20)
    {
      j = -1;
      while (++j < 20)
	{
	  sf::RectangleShape box(sf::Vector2f(40, 40));
	  box.setFillColor(sf::Color(20, 20, 20));
	  box.setOutlineColor(sf::Color(r, 45, b));
	  box.setOutlineThickness(-1);
	  box.setPosition(4 + (j * 40) - 20, 4 + (i * 40) - 20);
	  r += 0.360111;
	  b -= 0.360111;
	  this->_window.draw(box);
	}
    }
  this->_window.draw(borderH);
  borderH.setPosition(0,764);
  this->_window.draw(borderH);
  this->_window.draw(borderV);
  this->_window.draw(player1);
  this->_window.draw(player2);
  r = 70.0;
  b = 200.0;
  _redText.setString("Paws captured :" + _arb->getRedPawsTaken());
  _window.draw(_redText);
  _blueText.setString("Paws captured :" + _arb->getBluePawsTaken());
  _window.draw(_blueText);
  if (_arb->isBlueTurn())
    _window.draw(_yourTurnBlue);
  else
    _window.draw(_yourTurnRed);
 return (true);
}

bool			Gomoku::displayPawn()
{
  int			i = -1;
  int			j;

  while (++i < 19)
    {
      j = -1;
      while (++j < 19)
	{
	  if (this->_map[i*19+j%19]->isEmpty())
	    continue;
	  else if (this->_map[i*19+j%19]->isBlue())
	    {
	      sf::Sprite		bluePawn(this->_textures->getTexturesGame()[BLUE_PAWN]);
	      bluePawn.move(4 + (j * 40), 4 + (i * 40));
	      this->_window.draw(bluePawn);
	      continue;
	    }
	  else if (this->_map[i*19+j%19]->isRed())
	    {
	      sf::Sprite		redPawn(this->_textures->getTexturesGame()[RED_PAWN]);
	      redPawn.move(4 + (j * 40), 4 + (i * 40));
	      this->_window.draw(redPawn);
	      continue;
	    }
	  else
	    throw (std::string("Unexpected error while drawing."));
	}
    }
  if (_arb->isOver() && _arb->blueWin())
    this->displayTextureInterface(BLUE_WIN, 1024/2-454/2, 768/8-104/2);
  else if (_arb->isOver() && !_arb->blueWin())
    this->displayTextureInterface(RED_WIN, 1024/2-454/2, 768/8-104/2);
      return (true);
}

bool			Gomoku::displayTextureGame(eTextureGame tex, int x, int y)
{
  sf::Sprite sp(this->_textures->getTexturesInterface()[tex]);
  sp.move(x, y);
  this->_window.draw(sp);
  return (true);
}

bool			Gomoku::runInterface()
{
  while (this->_window.isOpen())
  {
    this->_window.clear();
    this->gestEventInterface();
    this->displayInterface();
    this->_window.display();
  }
  return (true);
}

bool			Gomoku::runGame()
{
  int			i;
  int			j;

  this->_arb = new Arbiter(this->_map, true, _pvp);
  this->_map.clear();
  for (i = 0; i < 20; ++i)
    for (j = 0; j < 20; ++j)
      this->_map.push_back(new Cell);
  if (!_pvp)
    this->_ai = new AI(_map, _maxAITime);
  while (this->_window.isOpen() && !this->_exit)
    {
      this->_window.clear(sf::Color(206, 206, 206));
      if (!_pvp && !_arb->isBlueTurn())
	{
	  _ai->play(false);
	  _arb->changeTurn();
	}
      else
	this->gestEventGame();
      this->displayGame();
      this->displayPawn();
      this->_window.display();
    }
  delete(this->_arb);
  if (!_pvp)
    delete(this->_ai);
  return (true);
}

void			Gomoku::take(int index)
{
  if (index < 17*19 && _map[index + 19]->isBlue() == !_arb->isBlueTurn() && _map[index + 19*3]->isRed() == !_arb->isBlueTurn() && !_map[index + 19]->isEmpty() && !_map[index + 19*2]->isEmpty() && !_map[index + 19*3]->isEmpty())
    {
      _map[index + 19]->setEmpty();
      _map[index + 19*2]->setEmpty();
      _arb->addTake();
    }
  if (index%19 < 17 && _map[index + 1]->isBlue() == !_arb->isBlueTurn() && _map[index + 2]->isBlue() == !_arb->isBlueTurn() && _map[index + 3]->isRed() == !_arb->isBlueTurn() && !_map[index + 1]->isEmpty() && !_map[index + 2]->isEmpty() && !_map[index + 3]->isEmpty())
    {
      _map[index + 1]->setEmpty();
      _map[index + 2]->setEmpty();
      _arb->addTake();
    }
  if (index > 19*3 && _map[index - 19]->isBlue() == !_arb->isBlueTurn() && _map[index - 19*2]->isBlue() == !_arb->isBlueTurn() && _map[index - 19*3]->isRed() == !_arb->isBlueTurn() && !_map[index - 19]->isEmpty() && !_map[index - 19*2]->isEmpty() && !_map[index - 19*3]->isEmpty())
    {
      _map[index - 19]->setEmpty();
      _map[index - 19*2]->setEmpty();
      _arb->addTake();
    }
  if (index%19 > 2 && _map[index - 1]->isBlue() == !_arb->isBlueTurn() && _map[index - 2]->isBlue() == !_arb->isBlueTurn() && _map[index - 3]->isRed() == !_arb->isBlueTurn() && !_map[index - 1]->isEmpty() && !_map[index - 2]->isEmpty() && !_map[index - 3]->isEmpty())
    {
      _map[index - 1]->setEmpty();
      _map[index - 2]->setEmpty();
      _arb->addTake();
    }
}
