//
// LoadTexture.cpp<2> for  in /home/switi/Documents/ai-gomoku
//
// Made by Switi
// Login   <switi@epitech.net>
//
// Started on  Tue Oct 21 18:35:18 2014 Switi
// Last update Fri Dec 26 19:21:02 2014 Lazio
//

#include "LoadTexture.hh"

LoadTexture::LoadTexture()
{
  if (!this->loadInterface())
    throw (std::string("Cannot load interface"));
  if (!this->loadGame())
    throw (std::string("Cannot load game"));
}

LoadTexture::~LoadTexture()
{

}

const std::vector<sf::Texture>	&LoadTexture::getTexturesInterface() const
{
  return (this->texturesInterface);
}

const std::vector<sf::Texture>	&LoadTexture::getTexturesGame() const
{
  return (this->texturesGame);
}

bool		LoadTexture::loadInterface()
{
  std::vector<std::string>		files;

  files.push_back(std::string("./media/img/title.png"));
  files.push_back(std::string("./media/img/blue_win.png"));
  files.push_back(std::string("./media/img/red_win.png"));
  files.push_back(std::string("./media/img/pvp.png"));
  files.push_back(std::string("./media/img/pve.png"));
  for (std::vector<std::string>::iterator it = files.begin(); it != files.end(); ++it)
    {
      sf::Texture			tmp;
      if (!tmp.loadFromFile(*it))
	return (false);
      this->texturesInterface.push_back(tmp);
    }
  return (true);
}

bool		LoadTexture::loadGame()
{
  std::vector<std::string>		files;

  files.push_back(std::string("./media/img/blue-pawn.png"));
  files.push_back(std::string("./media/img/red-pawn.png"));
  for (std::vector<std::string>::iterator it = files.begin(); it != files.end(); ++it)
    {
      sf::Texture			tmp;
      if (!tmp.loadFromFile(*it))
	return (false);
      this->texturesGame.push_back(tmp);
    }
  return (true);
}
